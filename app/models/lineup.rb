class Lineup < ApplicationRecord
  belongs_to :listing
  belongs_to :artist
end
