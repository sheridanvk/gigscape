class Artist < ApplicationRecord
    has_many :lineups
    has_many :listings, through: :lineups

    validates :songkick_id, :uniqueness => true

    before_save :get_spotify_details, unless: :spotify_artist_id?,
      if: Proc.new { |artist| !artist.is_human_verified? }

    private
    def get_spotify_details
      artist_details = Spotify.fetch_artist_details(name)

      if artist_details
        self.spotify_artist_id = artist_details.fetch(:spotify_artist_id)
        self.save
      end
    end
end
