require "httparty"
require "date"

module Songkick

  def self.fetch_listings(lat, lng, date)
    response = fetch("/events.json",
      {
        :type => "Concert", # we only want concerts, not festivals
        :location => "geo:#{lat.to_s},#{lng.to_s}",
        :min_date => date.to_s,
        :max_date => (date+1).to_s,
        :per_page => 100,
      })

    listings = response.fetch("resultsPage").fetch("results", nil).fetch("event", nil)

    if listings
      listings.map do |listing|
        # Get the venue information first, so that the loop can just be exited if
        # lat or lng is invalid
        venue_name = listing.fetch("venue").fetch("displayName")
        lat = listing.fetch("venue").fetch("lat").to_f
        lng = listing.fetch("venue").fetch("lng").to_f
        listing_id = listing.fetch("id")
        status = listing.fetch("status")

        next if lat == 0.0 || lng == 0.0 || status == "cancelled"

        performances = listing.fetch("performance")

        artists = performances.map do |perf|
          name = perf.fetch("artist").fetch("displayName")
          artist_id = perf.fetch("artist").fetch("id")

          # "billing" is either "headline" or "support".
          is_headline = perf.fetch("billing") == "headline"
          {
            name: name,
            songkick_id: artist_id,
            is_headline: is_headline,
          }
        end

        start_time = listing.fetch("start").fetch("datetime")
        parsed_start_time = Time.parse(start_time) if !start_time.nil?
        parsed_start_date = Date.parse(listing.fetch("start").fetch("date"))
        listing_url = listing.fetch("uri")

        {
          artists: artists,
          location: {
            :venue_name => venue_name,
            :lat => lat,
            :lng => lng,
          },
          start_time: parsed_start_time,
          start_date: parsed_start_date,
          listing_url: listing_url,
          songkick_id: listing_id,
        }
      end.compact
    end
  end

  def self.fetch(path, options)
    options.merge!(:apikey => ENV["SONGKICK_API_KEY"])
    puts "Songkick API: #{ENV["SONGKICK_API_KEY"]}"
    HTTParty.get("http://api.songkick.com/api/3.0#{path}", query: options)
  end

end
