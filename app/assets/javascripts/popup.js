'use strict';


const ensureElementInViewport = (el) => {
  // start by getting the current lat/lng of the center of the map
  // in lat/lng and pixels
  const center = mapEl.getCenter();
  const centerPixels = mapEl.project(center); // returns an x, y pair

  // see whether the popup extends outside of the map window, and build a
  // transformation matrix (in pixels) to move it if it does
  const rect = el.getBoundingClientRect();

  // check if a change in latitude is needed first
  const pixelPadding = 5;
  let latChangePixels = 0;
  if (rect.top < 0) {
    latChangePixels = rect.top - pixelPadding;
  } else if (rect.bottom > window.innerHeight) {
    latChangePixels = rect.bottom - window.innerHeight + pixelPadding;
  }

  // check longitude next
  let lngChangePixels = 0;
  if (rect.left < 0) {
    lngChangePixels = rect.left - pixelPadding;
  } else if (rect.right > window.innerWidth) {
    lngChangePixels = rect.right - window.innerWidth + pixelPadding;
  };

  if (latChangePixels != 0 || lngChangePixels != 0) {
    // convert pixels to lat/lng
    const newCenterPixels = [
      lngChangePixels + centerPixels.x,
      latChangePixels + centerPixels.y
    ];

    const newCenter = mapEl.unproject(newCenterPixels)

    mapEl.easeTo({center: newCenter});
  }
}
