class PopUp extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      listing: {},
      isLoaded: false
    };

    fetch(Routes.listing_path(this.props.id))
    .then(response => {
      return response.json()
    })
    .then(json => {
      this.setState({
         listing: JSON.parse(json.listing),
         isLoaded: true
      })
    })
  }

  render() {
    const {isLoaded} = this.state;

    return (
      <div>
        {
          isLoaded
          ? <div><Carousel listing={this.state.listing}/>
          </div>
          : <div>'Loading...' </div>
        }
      </div>
    )
  }
}
