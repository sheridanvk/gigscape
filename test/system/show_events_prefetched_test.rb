require 'application_system_test_case'

class ShowEventsPrefetchedTest < ApplicationSystemTestCase
  test 'Show events for Berlin' do
    visit '/'
    assert_selector('.mapboxgl-marker', wait: 10)
  end

  # test 'Show events for London' do
  #   visit '/'
  #   search_box = find('.mapboxgl-ctrl-geocoder input').native
  #
  #   search_box.send_keys('London, UK')
  #   search_box.send_keys(:return)
  #
  #   assert_selector('.mapboxgl-marker', wait: 10)
  # end
end
