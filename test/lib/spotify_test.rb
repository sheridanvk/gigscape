require 'test_helper'
require 'minitest/mock'

class SpotifyTest < ActiveSupport::TestCase
  response = JSON.parse(File.read('test/fixtures/files/spotify.json'))

  test 'fetching details about the artist from Spotify' do
    Spotify.stub(:fetch, response) do
      artist = Spotify.fetch_artist_details('Karla Kane')

      assert_equal(
        artist,
        spotify_artist_id: '7uVusXjLiQz6RYLLI0yYIA',
        :avatar_url => [
          {
            'height' => 640,
            'url' => 'https://i.scdn.co/image/d7fbd1c0cdcc6c15da8719760db9659c20e4383d',
            'width' => 640
          }, {
            'height' => 300,
            'url' => 'https://i.scdn.co/image/397a6ed2681e4a40b4c3e86d3002cf7db0f29c70',
            'width' => 300
          }, {
            'height' => 64,
            'url' => 'https://i.scdn.co/image/da43298d1a9149d6732676e31fd6d798089243e2',
            'width' => 64
          }
        ],
        :top_tracks_uri => 'spotify:artist:7uVusXjLiQz6RYLLI0yYIA'
      )
    end
  end
end
