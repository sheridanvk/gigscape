desc 'This task is called by the Heroku scheduler add-on'
task :get_listings => :environment do
  puts 'Getting today\'s listings...'

  cities = City.all
  date = Date.today

  cities.each do |city|
    puts "Getting listings for #{city.name}"
    ListingRetrievalService.new().download_listings_from_provider(city.lat, city.lng, date)
  end

  puts "done."
end

desc 'This task is called by the Heroku scheduler add-on'
task :delete_old_listings => :environment do
  puts "Deleting listings older than today"
  ListingDeletionService.new().remove_old_listings()
end

desc "Switch logger to stdout"
task :to_stdout => [:environment] do
 Rails.logger = Logger.new(STDOUT)
end
