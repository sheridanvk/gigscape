# This file should contain all the record creation needed to seed the database
# with its default values. The data can then be loaded with the rails db:seed
# command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([
#     { name: 'Star Wars' },
#     { name: 'Lord of the Rings' }
#   ])
#   Character.create(name: 'Luke', movie: movies.first)

city_list = [
    {name: 'Berlin', lat: 52.5200, lng: 13.4050},
    {name: 'New York City', lat: 40.7128, lng: -74.0060},
    {name: 'San Francisco', lat: 37.7749, lng: -122.4194},
    {name: 'London', lat: 51.5074, lng: -0.1278},
    {name: 'Detroit', lat: 42.3640, lng: -83.0883},
    {name: 'Paris', lat: 48.8566, lng: 2.3522},
    {name: 'Amsterdam', lat: 52.3702, lng: 4.8952},
    {name: 'Bristol', lat: 51.4545, lng: -2.5879},
    {name: 'Toronto', lat: 43.6532, lng: -79.3832},
    {name: 'Tokyo', lat: 35.6895, lng: 139.6917},
    {name: 'Reading', lat: 51.4543, lng: -0.9781},
    {name: 'Los Angeles', lat: 34.0522, lng: -118.2437},
    {name: 'Adelaide', lat: -34.9285, lng: 138.6007},
    {name: 'Singapore', lat: 1.3521, lng: 103.8198},
    {name: 'Zurich', lat: 47.3769, lng: 8.5417},
    {name: 'Dublin', lat: 53.3498, lng: -6.2603},
    {name: 'Brighton', lat: 50.8225, lng: -0.1372},
    {name: 'Auckland', lat: -36.8485, lng: 174.7633},
    {name: 'San Jose', lat: 37.3382, lng: -121.8863},
]

city_list.each do |city|
  City.create( name: city.fetch(:name), lat: city.fetch(:lat), lng: city.fetch(:lng) )
end
