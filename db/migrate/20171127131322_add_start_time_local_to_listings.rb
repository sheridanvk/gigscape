class AddStartTimeLocalToListings < ActiveRecord::Migration[5.1]
  def change
    add_column :listings, :start_time_local, :datetime
  end
end
