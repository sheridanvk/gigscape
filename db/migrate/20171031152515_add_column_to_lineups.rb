class AddColumnToLineups < ActiveRecord::Migration[5.1]
  def change
    add_column :lineups, :listing_id, :integer
    add_column :lineups, :artist_id, :integer
    add_column :lineups, :is_headline, :boolean
  end
end
