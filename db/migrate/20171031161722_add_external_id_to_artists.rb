class AddExternalIdToArtists < ActiveRecord::Migration[5.1]
  def change
    add_column :artists, :songkick_id, :string
  end
end
